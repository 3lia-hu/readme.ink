app.controller('mainCtrl', ['$scope','$window','$http','UserService',function($scope,$window,$http,UserService)
		{			
		//<init>
			$scope.updateMessage = "Setting up";
			$('#spinner').show();
			$('#closeIcon').hide();
			UserService.getData()
				.then(function(data)
						{
							$scope.userData = data;
							$scope.updateMessage = "Ready.";
							$('#spinner').hide();
						})
				.catch(function(err)
						{
							$window.location.href = "https://v1-readmedotink.rhcloud.com/login";
						});
		//</init>
			
			$scope.updateTargetUrl = function()
			{
				$scope.updateMessage = "Processing";
				$('#spinner').show();
				$('#closeIcon').hide();

								$http.get('updatetargeturl?noredirect=true&newtarget='+$scope.userData.target)
									.then(
											function successCallBack(response)
												{
													UserService.getData()
														.then(function(data)
															{
																$scope.userData = data;
																$scope.updateMessage = "Success!";
																$('#spinner').hide();
																$('#closeIcon').show();
															})
														.catch(function(err)
															{
																$window.location.href = "https://v1-readmedotink.rhcloud.com/login";
															});
												},
											function errorCallBack(response)
												{
													$scope.updateMessage = "Invalid url.";
													$('#spinner').hide();
													$('#closeIcon').show();
												});
			};
			$scope.resetStatus = function()
				{
					if ($scope.updateMessage != "Processing")
						{
							$('#closeIcon').hide();
							$scope.updateMessage = "Ready.";
						}
				};
		}
]);

app.controller('settingsCtrl', ['$scope','$window','$http','UserService','ValidationService',function($scope,$window,$http,UserService,ValidationService)
                    		{
								//<init>
									UserService.getData()
										.then(function(data)
												{
													$scope.userData = data;
												})
										.catch(function(err)
												{
													$window.location.href = "https://v1-readmedotink.rhcloud.com/login";
												});
									
									$scope.validPassword = true;	//init as TRUE - HIDDEN in html
									$scope.passwordsMatch = true;
									$('#closeIcon').hide();
									$('#spinner').hide();
									$scope.updateMessage = "";
								//</init>
									
									$scope.validatePassword = function()
											{
												$scope.validPassword = ValidationService.validatePassword($scope.password.newp);
											}
									$scope.validatePasswordMatch = function ()
											{
												$scope.passwordsMatch = ValidationService.validatePasswordMatch($scope.password.newp,$scope.password.confirm);
											}
									$scope.updatePassword = function()
											{
												$('#closeIcon').hide();
												$('#spinner').show();
												$scope.updateMessage = "";
												$scope.password.email = $scope.userData._id;
												var headers = {'Content-Type': 'application/x-www-form-urlencoded'};
												//$http.post('updatepassword',$scope.password,headers)
												$http({
														method: 'POST',
														url: 'updatepassword',
														headers: {'Content-Type': 'application/x-www-form-urlencoded'},
														transformRequest: function(obj) {
															var str = [];
															for(var p in obj)
																str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
															return str.join("&");
														},
														data: $scope.password
													})
													.then(
															function successCallBack(response)
																{
																	$('#closeIcon').show();
																	$('#spinner').hide();
																	$scope.updateMessage = "Done.";
																	console.log(response);
																},
															function errorCallBack(response)
																{
																	$('#closeIcon').show();
																	$('#spinner').hide();
																	if (response.statusText=="Unauthorized")
																		{
																			$scope.updateMessage = "Password incorrect."
																		}
																	else
																		{
																			console.log(response);
																			$scope.updateMessage = response.statusText;
																		}
																}
														);
											}
									$scope.resetStatus = function()
										{
											$('#closeIcon').hide();
											$scope.updateMessage = "";												
										};
                    		}
]);


app.controller('helpCtrl', ['$scope','$window','$http','UserService',function($scope,$window,$http,UserService)
                    		{
								UserService.getData()
										.then(function(data)
												{
													$scope.userData = data;
												})
										.catch(function(err)
												{
													$window.location.href = "https://v1-readmedotink.rhcloud.com/login";
												});
                    		}
]);

app.controller('labCtrl', ['$scope','$window','$http','UserService',function($scope,$window,$http,UserService)
                    		{
								UserService.getData()
										.then(function(data)
												{
													$scope.userData = data;
												})
										.catch(function(err)
												{
													$window.location.href = "https://v1-readmedotink.rhcloud.com/login";
												});
                    		}
]);