app.factory('UserService',['$q', '$http',
                function ($q, $http)
                    {
                        // create user variables
                        var Data = null;
                        // return available functions for use in controllers
                        return ({
                            isLoggedIn: isLoggedIn,
                            getData: getData,
                            Data: Data});
                        
                        function isLoggedIn()
                        	{
                        		if(Data!=null) {return true;}
                        		else {return false;}
                        	}
                        
                        function getData()
                        	{
                        		var deferred = $q.defer();
                        		$http.get('/userdata')
                        			.then(
                        					function successCallback(response)
                        						{
                        							Data = response.data;
                        							return deferred.resolve(Data);
                        						},
                        					function errorCallback(response)
                        						{
                        							return deferred.reject(response);
                        						});
                        		return deferred.promise;
                        	}
}]);

app.service('ValidationService', function()
		{
			this.validateEmail = function (email)
				{
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					//regex accepting unicode
					//var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
					return re.test(email);
				};
			
			this.validatePassword = function(password)
   				{
   					var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
   					return re.test(password);
   				};
   				
   			this.validatePasswordMatch = function (password,passwordconfirm)
   				{
	   				return (password===passwordconfirm);
   				};
		}
);