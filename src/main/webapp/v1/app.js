var app = angular.module('v1',['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        
        .when('/', {
            templateUrl : 'v1/views/main.html',
            controller  : 'mainCtrl',
            access: {restricted: true}
        })
        
        .when('/help', {
            templateUrl : 'v1/views/help.html',
            controller  : 'helpCtrl',
            access: {restricted: true}
        })

        .when('/lab', {
            templateUrl : 'v1/views/lab.html',
            controller  : 'labCtrl',
            access: {restricted: true}
        })

        .when('/settings', {
            templateUrl : 'v1/views/settings.html',
            controller  : 'settingsCtrl',
            access: {restricted: true}
        });
});

app.directive('ebButtons', function()
		{
	return {
			restrict: 'C',
			templateUrl: '/v1/componens/eb-buttons.html'
		   };
}
);