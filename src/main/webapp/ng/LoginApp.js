/**
 * 
 */

var app = angular.module('LoginApp', []);

app.controller('resetPwdCtrl',['$scope','$http', function($scope,$http)
                   {
					//<init>
						$scope.pswResetForm=false;
						$scope.updateMessage = "";
						$('#spinner').hide();
						$('#closeIcon').hide();
					//</init>
					//<form show and hide functions>
						$scope.ShowHideResertForm = function()
							{
								$scope.pswResetForm = !$scope.pswResetForm;
							};
						$scope.resetStatus = function()
							{
								if ($scope.updateMessage != "Processing")
									{
										$('#closeIcon').hide();
										$scope.updateMessage = "";
									}
							};
					//</form show and hide functions>
					//<main ajax function>
						$scope.resetPassword = function()
							{
								//<GUI init>
									$scope.updateMessage = "Processing";
									$('#spinner').show();
									$('#closeIcon').hide();
								//</GUI init>
								//<ajax function to reset password based on email>
									$http.get('/passwordreset?email='+$scope.email)
                        			.then(
                        					function successCallback(response)
                        						{
                        							$scope.updateMessage = "Done! Check your inbox";
                        							$('#spinner').hide();
                        							$('#closeIcon').show();
                        						},
                        					function errorCallback(response)
                        						{
                        							$scope.updateMessage = response.statusText;
                									$('#spinner').hide();
                									$('#closeIcon').show();
                									if (response.statusText=="Bad Request")
														{
															$scope.updateMessage = "Email address not registered"
														}
                									else
														{
															$scope.updateMessage = response.statusText;
														}
                        						}
                        					);
								//</ajax function to reset password based on email>
							}
					//</main ajax function>
                   }
]);