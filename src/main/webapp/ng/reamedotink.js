/**
 * 
 */

var app = angular.module('reamedotink', []);

app.controller('containerCtrl', function($scope)
		{
			$scope.loginVisible = true;
			$scope.registrationVisible = false;
			$scope.toggleView = function ()
				{
					$scope.loginVisible = ! $scope.loginVisible;
					$scope.registrationVisible = ! $scope.registrationVisible;
				};
		}
);

app.controller('loginCtrl', function($scope,$http)
		{
			$scope.loginError = false;
		}
);

app.controller('registrationCtrl', function($scope)
		{
			$scope.registrationError = false;
			$scope.emailOK = false;
			$scope.passwordOK = false;
			$scope.matchOK = false;
			$scope.checkEmail = function()
									{
										var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
										//regex accepting unicode
										//var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
										if(!re.test($scope.email))
											{
												$scope.emailError = "Invalid email address";
												$scope.emailOK = false;
											}
										else
											{
												$scope.emailError = "";
												$scope.emailOK = true;
											}
									};
			$scope.checkPassword = function()
									{
										// at least one number, one lowercase and one uppercase letter
										// at least six characters that are letters, numbers
										var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
										if(!re.test($scope.password))
											{
												$scope.msg = {Alert:"Invalid password",Text:"It must be at least 6 characters, with one upper and lowercase letter and one number",Icon:"glyphicon glyphicon-exclamation-sign"};
												$scope.passwordOK = false;
											}
										else
											{
												$scope.msg = {Alert:"",Text:"",Icon:""};
												$scope.passwordOK = true;
											}
									};
			$scope.matchPasswords = function()
										{
											if($scope.password != $scope.Cnfpassword)
												{
													$scope.PasswordsnotMatch = "Passwords are not matching";
													$scope.matchOK = false;
												}
											else
												{
													$scope.PasswordsnotMatch = "";
													$scope.matchOK = true;
												}
											};
			$scope.isDisabled = function() { return !($scope.emailOK && $scope.passwordOK && $scope.matchOK && $scope.tac); };
		}
);