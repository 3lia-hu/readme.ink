/**
 * 
 */

var app = angular.module('updateTarget', []);

app.controller('updateForm', function($scope,$http)
		{
			$scope.target = $scope.target;  
			$scope.send = function()
				{
					$http.get('updatetargeturl?noredirect=true&newtarget='+$scope.target)
							.then(
									function successCallBack(response)
										{
											alert("Sucess " + response.data);
										},
									function errorCallBack(response)
										{
											alert("Error " + response.data);
										}
								);
				};
		}
);