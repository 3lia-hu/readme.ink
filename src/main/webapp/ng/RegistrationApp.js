var app = angular.module('RegistrationApp', []);

app.controller('registrationformCtrl',['$scope','ValidationService', function($scope,ValidationService)
							{
								$scope.emailOK = true;
								$scope.passwordOK = true;
								$scope.matchOK = true;
								$scope.checkEmail = function()
														{
															$scope.emailOK = ValidationService.validateEmail($scope.email);
														};
								$scope.checkPassword = function()
														{
															$scope.passwordOK = ValidationService.validatePassword($scope.password);
														};
								$scope.matchPasswords = function()
															{
																$scope.matchOK = ValidationService.validatePasswordMatch($scope.password,$scope.Cnfpassword);
															};
								$scope.isDisabled = function() { return !($scope.emailOK && $scope.passwordOK && $scope.matchOK && $scope.tac); };
							}
]);

app.service('ValidationService', function()
		{
			this.validateEmail = function (email)
				{
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					//regex accepting unicode
					//var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
					return re.test(email);
				};
			
			this.validatePassword = function(password)
   				{
   					var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
   					return re.test(password);
   				};
   				
   			this.validatePasswordMatch = function (password,passwordconfirm)
   				{
	   				return (password===passwordconfirm);
   				};
		}
);
