package ink.readme;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;

import ink.readme.model.UserDAO;

@WebServlet(urlPatterns={"/passwordreset"})
public class PasswordReset extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			String email = request.getParameter("email");
			String password = RandomStringUtils.randomAlphanumeric(17);
			
			UserDAO userDAO = new UserDAO(MongoDB);			
			
			if (userDAO.updatePassword(email, password))
				{
					EmailUtility.sendEmail(email, "[support] ReadME.ink password reset", "<h1>Hi!</h1><br><p> Here it is, log in and change it immediately: <b>"+password+"</b></p><p><i>the ReadME.ink Support Team</i></p>");
					response.setContentType("application/json; charset=UTF-8");
					PrintWriter writer = response.getWriter();
					writer.print("{\"status\":\"ok\"}");
					writer.flush();
					writer.close();
				}
			else
				{
					response.sendError(400,"User not found");
				}
		}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			response.sendError(400,"Bad Request");//TODO migliorare errori
		}

}
