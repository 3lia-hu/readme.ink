package ink.readme;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
public class EmailUtility {
    public static void sendEmail(String toAddress, String subject, String message) {
 
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtps.aruba.it");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.auth", "true");
 
        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("support@readme.ink", "marzo2015");
            }
        };
 
        Session session = Session.getInstance(properties, auth);
 
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        try {
        		msg.setFrom(new InternetAddress("Support@readme.ink"));
        		InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        		msg.setRecipients(Message.RecipientType.TO, toAddresses);
        		msg.setSubject(subject);
        		msg.setSentDate(new Date());
        		msg.setContent(message, "text/html; charset=utf-8");
        		Transport.send(msg);
        		
        } catch (AddressException e) {
        	throw new RuntimeException(e);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
 
    }
}
