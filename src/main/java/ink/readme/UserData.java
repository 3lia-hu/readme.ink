package ink.readme;

import ink.readme.model.UserDAO;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/userdata")
public class UserData extends ServletFactory {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			String email = getSessionUserEmail(request);
			if (email == null)
				{
					response.sendError(400);
				}
			
			UserDAO userDAO = new UserDAO(MongoDB);
			Document user = userDAO.getUserbyEmail(email);
			
			if (user == null) {response.sendError(500);}
			else
				{
					response.setContentType("application/json; charset=UTF-8");
					response.setCharacterEncoding("UTF-8");
					PrintWriter writer = response.getWriter();
					writer.print(user.toJson());
					writer.flush();
					writer.close();
				}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
