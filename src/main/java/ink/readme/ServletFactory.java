package ink.readme;

import ink.readme.model.SessionDAO;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Abstract Servlet for FreeMarker and MongoDB in OpenShift
 * 
 * @autor Elia Biraschi
 * @version 1.0
 */
public abstract class ServletFactory extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//Path to the templates in OpenShift
	private final static File TemplatesPath = new File(System.getenv("OPENSHIFT_REPO_DIR")+"/src/main/webapp/WEB-INF/templates");
	//configuration for FreeMarker
	private final Configuration FreeMarkerCnf = new Configuration();
	//MongoDB full URL in OpenShift
	private final static String MongoDBUrl = System.getenv("OPENSHIFT_MONGODB_DB_URL");
	//Mongo DB Name
	private final static String DBName = "v1";
	//MongoDB client
	MongoClient Mongoclient;
	//MongoDB Object to pass to DAOs objects
	protected MongoDatabase MongoDB;
	//Session Cookie name
	protected final static String sessionCookieName = "Gear0SessionReadmedotink";
	//basic pages paths
	protected final static String loginPage = "/login";
	protected final static String logoutPage = "/logout";
	protected final static String userPage = "/v1";
	protected final static String indexPage = "/index.html";
	protected final static String registerPage = "/register";

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException
		{
			//setup configuration for Freemarker
			try 
				{
					FreeMarkerCnf.setDirectoryForTemplateLoading(TemplatesPath);
					FreeMarkerCnf.setDefaultEncoding("UTF-8");
					FreeMarkerCnf.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER); //TODO change debug mode with public deployment setting
				}
			catch (IOException e) { e.printStackTrace(); } //TODO handle exception
			
			Mongoclient = new MongoClient(new MongoClientURI(MongoDBUrl));
			MongoDB = Mongoclient.getDatabase(DBName);
		}
	
	public void destroy() {
		Mongoclient.close();
	}
	
	protected void printOut(HttpServletResponse R, Map<String, Object> M, String T) throws IOException
		{
			Template template = FreeMarkerCnf.getTemplate(T);
			R.setContentType("text/html; charset=UTF-8");
			R.setCharacterEncoding("UTF-8");
			PrintWriter writer = R.getWriter();		
			try { template.process(M, writer); }
			catch (TemplateException e)	{ writer.println(e.getMessage()); }
			finally { writer.close(); }
		}
	
	protected void startSession(String email, HttpServletResponse response)
		{
			SessionDAO SessionDAO = new SessionDAO(MongoDB);
			String sessionID = SessionDAO.startSession(email);
			Cookie c1 = new Cookie(sessionCookieName, sessionID);
			c1.setMaxAge(60*60*24*7); //cookie expiration: 1 week
			response.addCookie(c1);
			
			/*Cookie c2 = new Cookie(sessionCookieName, sessionID);
			c2.setMaxAge(60*60*24*7); //cookie expiration: 1 week
			c2.setDomain(".readmedotink2-devhubelia.rhcloud.com");
			response.addCookie(c2);*/
		}
	
	protected String getSessionUserEmail(HttpServletRequest request)
		{
			SessionDAO sessionDAO = new SessionDAO(MongoDB);
			Cookie[] cookies = request.getCookies();
			if (cookies != null)
				{
					for (int i = 0; i < cookies.length; i++)
						{
							if (cookies[i].getName().equals(sessionCookieName))
			  					{
									String sessionId = cookies[i].getValue();
									return sessionDAO.findUserNameBySessionId(sessionId);
			  					}	
						}
				}
			return null;
		}
	
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
