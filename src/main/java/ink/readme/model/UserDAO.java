package ink.readme.model;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;

import org.bson.Document;
import org.bson.conversions.Bson;

import sun.misc.BASE64Encoder;

import com.mongodb.ErrorCategory;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import static java.util.Arrays.asList;

@SuppressWarnings("restriction")
public class UserDAO {
	
	private final MongoCollection<Document> usersCollection;
	private Random random = new SecureRandom();
	
	public UserDAO(final MongoDatabase Database)
		{
        	usersCollection = Database.getCollection("users");
		}
	
	public boolean addUser(String email, String password)
		{
			String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));
			String userID = String.valueOf(UUID.randomUUID());
			BitlyClient client = new BitlyClient("c41598f201d82d6e6f9de2a09fea62f7542b5540");
			Response<ShortenResponse> respShort = client.shorten() //
			    										.setLongUrl("https://v1-readmedotink.rhcloud.com/go?uID="+userID) //
			    										.call();
			if (respShort.status_code != 200)
				{
					throw new RuntimeException(); //TODO maybe better?
				}
			Document newUser = new Document("_id", email)
									.append("password", passwordHash)
									.append("userid",userID)
									.append("shorturl",respShort.data.url)
									.append("target", "https://en.wikipedia.org/wiki/Budapest")
									.append("urlslist", Arrays.asList());
			try
				{
	            	usersCollection.insertOne(newUser);
	            	return true;
				} 
			catch (MongoWriteException e)
				{
	            	if (e.getError().getCategory().equals(ErrorCategory.DUPLICATE_KEY))
	            		{
	            			System.out.println("Email already in use: " + email);
	            			return false;
	            		}
	            	throw e;
				}
		}
	
	public Document validateLogin(String email, String password)
		{
			Document user;
			user = usersCollection.find(Filters.eq("_id", email)).first();
			
			if (user == null)
				{
	            	System.out.println("User not in database");
	            	return null;
				}
			
			String hashedAndSalted = user.get("password").toString();
			String salt = hashedAndSalted.split(",")[1];
			
			if (!hashedAndSalted.equals(makePasswordHash(password, salt))) {
	            System.out.println("Submitted password is not a match");
	            return null;
	        }
			
			return user;			
		}
	
	public Document getUserbyEmail(String email)
		{
			Document projection = new Document("password",0);
			Document user;
			user = usersCollection.find(Filters.eq("_id", email))
									.projection(projection)
									.first();
			return user;
		}
	
	public String getTargetlbyUserID(String userID)
		{
			Document projection = new Document("_id",0).append("target",1);
			Document user = usersCollection.find(Filters.eq("userid", userID))
											.projection(projection)
											.first();
			if (user != null)
				{
					return (String) user.get("target");
				}
			else
				{
					return null;
				}
		}
	
	public void updateTargetUrl(String url, Document fullentry, String email)
		{
			Document list = new Document("$addToSet", new Document("urlslist", new Document("$each",asList(fullentry)).append("$slice",100) ) );			
			Document target = new Document("$set",new Document("target",url));
			usersCollection.updateOne(Filters.eq("_id",email), list);
			usersCollection.updateOne(Filters.eq("_id",email), target);
		}
	
	public boolean updatePassword(String email, String password)
		{			
			Bson emailFilter = Filters.eq("_id",email);
			Document user = usersCollection.find(emailFilter).first();
			if (user != null)
				{
					String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));
					Document passwordUpdate = new Document("$set", new Document("password", passwordHash));
					usersCollection.updateOne(emailFilter, passwordUpdate);
					
					return true;
				}
			else
				{
					return false;
				}
		}
	
	
	
	
	private String makePasswordHash(String password, String salt) 
		{
        	try {
        		String saltedAndHashed = password + "," + salt;
        		MessageDigest digest = MessageDigest.getInstance("MD5");
        		digest.update(saltedAndHashed.getBytes());
        		BASE64Encoder encoder = new BASE64Encoder();
        		byte hashedBytes[] = (new String(digest.digest(), "UTF-8")).getBytes();
        		return encoder.encode(hashedBytes) + "," + salt;
        	} catch (NoSuchAlgorithmException e) {
        		throw new RuntimeException("MD5 is not available", e);
        	} catch (UnsupportedEncodingException e) {
        		throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
        	}
		}

}
