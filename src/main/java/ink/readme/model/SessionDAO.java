package ink.readme.model;

import java.security.SecureRandom;

import org.bson.Document;

import sun.misc.BASE64Encoder;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

@SuppressWarnings("restriction")
public class SessionDAO {
	
	private final MongoCollection<Document> sessionsCollection;

    public SessionDAO(final MongoDatabase database)
    	{
        	sessionsCollection = database.getCollection("sessions");
    	}
    
    public String findUserNameBySessionId(String sessionId)
    	{
    		Document session = getSession(sessionId);
    		if (session == null)
    			{
    				return null;
    			} 
    		else 
    			{
    				return session.get("email").toString();
    			}
    	}
    
    // starts a new session in the sessions table
    public String startSession(String email)
    	{
    		//get 32 byte random number. that's a lot of bits.
        	SecureRandom generator = new SecureRandom();
        	byte randomBytes[] = new byte[32];
        	generator.nextBytes(randomBytes);
        	BASE64Encoder encoder = new BASE64Encoder();
        	String sessionID = encoder.encode(randomBytes);
        	//build the BSON object
        	Document session = new Document("email", email)
        							.append("_id", sessionID);
        	sessionsCollection.insertOne(session);
        	return session.getString("_id");
    	}
    
    // ends the session by deleting it from the sessions table
    public void endSession(String sessionID)
    	{
        	sessionsCollection.deleteOne(Filters.eq("_id", sessionID));
    	}
    //TODO manage sessions, close all, etc

    // retrieves the session from the sessions table
    public Document getSession(String sessionID)
    	{
        	return sessionsCollection.find(Filters.eq("_id", sessionID)).first();
    	}

}
