package ink.readme;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ink.readme.model.UserDAO;

@WebServlet(urlPatterns={"/UpdatePassword","/updatepassword"})
public class UpdatePassword extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
		{
			response.sendError(400,"Bad Request");
		}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
		{
			String password = request.getParameter("current");
			String newpassword = request.getParameter("newp");
			String passwordconfirm = request.getParameter("confirm");
			String email = request.getParameter("email");
					
			if (!newpassword.equals(passwordconfirm))
				{
					response.sendError(400,"Bad Request: New password invalid");
				}
			else
				{
					UserDAO userDAO = new UserDAO(MongoDB);	
					if (userDAO.validateLogin(email, password) != null)
						{
							userDAO.updatePassword(email, newpassword);
							response.setContentType("application/json; charset=UTF-8");
							//response.setCharacterEncoding("UTF-8");
							PrintWriter writer = response.getWriter();
							writer.print("{\"status\":\"ok\"}");
							writer.flush();
							writer.close();
						}
					else
						{
							response.sendError(401,"Authentication Error");
						}
				}
		}

}
