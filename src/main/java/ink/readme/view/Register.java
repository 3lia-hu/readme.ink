package ink.readme.view;

import ink.readme.ServletFactory;
import ink.readme.model.UserDAO;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns={"/Register","/register"})
public class Register extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			//if user already logged in redirects him/her to the userpage
			String userEmail = getSessionUserEmail(request);
			if (userEmail != null)
				{
					response.sendRedirect(userPage);
				}
			
			Map<String, Object> root = new HashMap<String, Object>();
			root.put("registrationError", false);
		
			printOut(response, root, "register.html");
		}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			Map<String, Object> root = new HashMap<String, Object>();
			
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			
			UserDAO userDAO = new UserDAO(MongoDB);
			//TODO add credential validation
			if(!userDAO.addUser(email, password))
				{
					// Duplicated email - registration FAILED
					root.put("registrationError", true);
					
					printOut(response, root, "register.html");
				}
			else
				{
					//Good credentials, start a new session
					startSession(email,response);
					response.sendRedirect(userPage);
				}
		}
}
