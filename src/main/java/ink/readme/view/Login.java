package ink.readme.view;

import ink.readme.ServletFactory;
import ink.readme.model.UserDAO;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns={"/Login","/login"})
public class Login extends ServletFactory
	{
		private static final long serialVersionUID = 1L;

		@Override
		protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
			{
				//if user already logged in redirects him/her to the userpage
				String userEmail = getSessionUserEmail(request);
				if (userEmail != null)
					{
						response.sendRedirect(userPage);
					}
				
				Map<String, Object> root = new HashMap<String, Object>();
				root.put("loginError", false);
	        
				printOut(response, root, "login.html");		
			}

		@Override
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException 
			{
				Map<String, Object> root = new HashMap<String, Object>();
				
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				
				UserDAO userDAO = new UserDAO(MongoDB);
				
				if (userDAO.validateLogin(email, password) != null)
					{
						//Login successful
						startSession(email,response);
						response.sendRedirect(userPage);
					}
				else 
					{
						root.put("loginError", true);
						printOut(response, root, "login.html");
					}
			}

	}
