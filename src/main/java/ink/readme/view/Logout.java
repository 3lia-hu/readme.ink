package ink.readme.view;

import ink.readme.ServletFactory;
import ink.readme.model.SessionDAO;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns={"/Logout","/logout"})
public class Logout extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			SessionDAO sessionDAO = new SessionDAO(MongoDB);
			Cookie[] cookies = request.getCookies();
			if (cookies != null)
				{
					for (int i = 0; i < cookies.length; i++)
							{
								if (cookies[i].getName().equals(sessionCookieName))
									{
										String sessionId = cookies[i].getValue();
										sessionDAO.endSession(sessionId);
									}		
							}
				}
			response.sendRedirect(loginPage);
		}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			response.sendError(400,"Bad Request");//TODO migliorare errori
		}

}
