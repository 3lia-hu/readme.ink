package ink.readme.view;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Servlet implementation class V1
 */
@WebServlet("/v1")
public class V1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final Configuration FreeMarkerCnf = new Configuration();
	private final static File TemplatesPath = new File(System.getenv("OPENSHIFT_REPO_DIR")+"/src/main/webapp/WEB-INF/templates");

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try 
			{
				FreeMarkerCnf.setDirectoryForTemplateLoading(TemplatesPath);
				FreeMarkerCnf.setDefaultEncoding("UTF-8");
				FreeMarkerCnf.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER); //TODO change debug mode with public deployment setting
			}
		catch (IOException e) { e.printStackTrace(); } //TODO handle exception
		
		Template template = FreeMarkerCnf.getTemplate("v1.html");
		Map<String, Object> M = new HashMap<String, Object>();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();		
		try { template.process(M,writer); }
		catch (TemplateException e)	{ writer.println(e.getMessage()); }
		finally { writer.close(); }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
