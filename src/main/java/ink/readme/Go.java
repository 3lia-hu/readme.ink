package ink.readme;

import ink.readme.model.UserDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;

@WebServlet(urlPatterns={"/Go","/go"})
public class Go extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			String targetUrl = request.getParameter("url");
			String userID = request.getParameter("uID");
			
			UserDAO userDAO = new UserDAO(MongoDB);
			String lasturl = userDAO.getTargetlbyUserID(userID);
			
			if (lasturl != null)
				{
					if (targetUrl == null)
						{
							targetUrl = lasturl;
						}
			
					response.setContentType("text/html; charset=UTF-8");
					response.setCharacterEncoding("UTF-8");
					PrintWriter writer = response.getWriter();
					writer.print(getParsedHTML(targetUrl,"relaxed",userID));
					writer.close();
				}
			else
				{
					response.sendRedirect(indexPage);
				}
			
		}

	private String getParsedHTML(String Url, String ParseMode, String userID) {
		Document SourceDoc;
		try
			{
				SourceDoc = Jsoup.connect(Url)
							 .userAgent("Mozilla")
							 .get();
			}
		catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		//redirects all the links through the app
		String redirectedLink;
		for (Element link : SourceDoc.select("a"))
			{
				try
					{
						redirectedLink = "https://v1-readmedotink.rhcloud.com/go?url="+URLEncoder.encode(link.absUrl("href"),"UTF-8")+"&uID="+userID;
						link.attr("href", redirectedLink);
					}
				catch (UnsupportedEncodingException e) 
					{
						throw new RuntimeException(e);
					}
			}
		//assigns abs links to the images src
		for (Element link : SourceDoc.select("img"))
			{
				link.attr("src", link.attr("abs:src"));
			}
		Charset charset = SourceDoc.outputSettings().charset();
		String title = SourceDoc.title();
		String CSS = "body{font-size:100%;line-height:1.5;font-family: 'Merriweather', Georgia, 'Times New Roman', Times, serif;max-width: 34em;margin:auto;padding:0 1em;} h1{font-size: 2.747em;} h2{font-size: 0.874em;} h3{font-size:1.229em;} img{max-width: 90%;} @media screen and (max-width: 25em) {body {font-size: 90%;}}";
		Whitelist WList;
		if (ParseMode.equals("relaxed"))
			{
				WList = Whitelist.relaxed();
			}
		else
			{
				WList = Whitelist.basicWithImages();
			}
		
		String parsedHTML = Jsoup.clean(SourceDoc.outerHtml(), WList);
		String source = ("<html><head><meta charset="+charset+"><title>"+title+"</title><style>"+CSS+"</style></head><body>"+parsedHTML+"</body></html>");
		
		return source;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			response.sendError(400,"Bad Request");//TODO migliorare errori
		}

}
