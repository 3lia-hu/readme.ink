package ink.readme;

import ink.readme.model.UserDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;

import org.bson.Document;

@WebServlet(urlPatterns={"/UpdateTargetUrl","/updatetargeturl"})
public class UpdateTargetUrl extends ServletFactory {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			//response.sendError(400,"Bad Request"); need to keep it as GET
			doPost(request, response);
		}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
		{
			String email = getSessionUserEmail(request);
			if (email == null)
				{
					response.sendRedirect(loginPage);
				}
			else
				{
					String newtarget = request.getParameter("newtarget");
					String pageTitle = validateUrl(newtarget);
					if (pageTitle == null)
						{
							response.sendError(400);
						}
					else
						{
							UserDAO userDAO = new UserDAO(MongoDB);
							Document entry = new Document("url",newtarget)
											.append("title",pageTitle)
											.append("date",new Date());
							userDAO.updateTargetUrl(newtarget, entry, email);
			
							String noredirect = request.getParameter("noredirect");
							if (noredirect == null)
								{
									response.sendRedirect(newtarget);
								}
							else
								{
									response.setContentType("application/json; charset=UTF-8");
									response.setCharacterEncoding("UTF-8");
									PrintWriter writer = response.getWriter();
									writer.print("{\"status\":\"ok\"}");
									writer.flush();
									writer.close();
								}
						}
				}
		}
	
	private String validateUrl(String url)
		{
			org.jsoup.nodes.Document SourceDoc;
			try
				{
					SourceDoc = Jsoup.connect(url)
								.userAgent("Mozilla")
								.get();
					return SourceDoc.title();
				}
			catch (IOException e)
				{
					return null;
				}
		}
}
